import { InjectionToken } from '@angular/core';

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig  = {
  routes: {
    printers: 'printers',
    error404: '404'
  },

  endpoints: {
    printers: 'http://127.0.0.1:8080/printers'
  },
};
