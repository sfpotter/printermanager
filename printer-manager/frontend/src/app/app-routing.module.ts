import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AppConfig} from './config/app.config';

const routes: Routes = [
  {path: '', loadChildren: 'app/printers/printers.module#PrintersModule'}
];


@NgModule({
    imports: [
      RouterModule.forRoot(routes,  { enableTracing: true } )
    ],
    exports: [
      RouterModule
    ]
  })

export class AppRoutingModule {
}
