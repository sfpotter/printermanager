import {Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-printers',
  templateUrl: './printers.component.html'
})

export class PrintersComponent {

  constructor(private activatedRoute: ActivatedRoute) {

  }
}
