import { Component, OnInit } from '@angular/core';
import {  Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-printer-add',
  templateUrl: './printer-add.component.html',
  styleUrls: ['./printer-add.component.css']
})
export class PrinterAddComponent  {
  public name = new FormControl('', [Validators.required]);

  public getErrorMessage() {
    return this.name.hasError('required') ? 'You must enter a name' : '';
  }
}
