import {ModuleWithProviders, NgModule} from '@angular/core';
import {MaterialModule} from './material.module';
import {TranslateModule} from '@ngx-translate/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import { PrinterService } from '../shared/printer.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    MaterialModule,
    FlexLayoutModule,
    TranslateModule
  ],
  exports: [
    MaterialModule,
    FlexLayoutModule,
    TranslateModule
  ]
})

export class ExternalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ExternalModule,
      providers: [
        PrinterService
      ]
    };
  }
}
