import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Printer } from '../shared/printer.model';
import { PrinterService } from '../shared/printer.service';

@Component({
    selector: 'app-printer-list',
    templateUrl: './printer-list.component.html',
    styleUrls: ['./printer-list.component.scss']
  })

export class PrinterListComponent {
    private printers: Printer[];
    private tableColumns: any;

    /**
     * @param activatedRoute
     */
    constructor(private activatedRoute: ActivatedRoute, private printerService: PrinterService) {
        this.tableColumns = ['name', 'ip', 'description', 'location', 'connection', 'status', 'actions'];
        this.printerService.getAll().subscribe((printers: Array<Printer>) => {
            this.printers = printers;
        });
    }
}
