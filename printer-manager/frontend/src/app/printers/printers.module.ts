import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { PrinterListComponent } from './printer-list/printer-list.component';
import { PrintersRoutingModule } from './printers-routing.module';
import { PrintersComponent } from './printers.component';
import { PrinterService } from './shared/printer.service';
import { ExternalModule } from './external/external.module';
import { PrinterAddComponent } from './printer-add/printer-add.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ExternalModule,
    PrintersRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    PrinterListComponent,
    PrintersComponent,
    PrinterAddComponent
  ],
  entryComponents: [
  ],
  providers: [
    PrinterService
  ]
})

export class PrintersModule {
}
