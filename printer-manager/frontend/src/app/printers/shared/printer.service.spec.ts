import { PrinterService } from './printer.service';

describe('Printer Service Unit Test', () => {
    it('should get printer by ID', () => {
        PrinterService.getPrinterById('2').subscribe((printerRow) => {
           expect(printerRow.id).toEqual(2);
        });
    });
});
