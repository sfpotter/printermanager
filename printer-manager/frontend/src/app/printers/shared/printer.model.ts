export class Printer {
    constructor(public name: string,
                public status: string,
                public address: string,
                public description: number,
                public netMask: string
            ) {
    }
  }
