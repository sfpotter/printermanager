import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {AppConfig} from '../../config/app.config';

import {Printer} from './printer.model';
import {Observable} from 'rxjs/Observable';
import {TranslateService} from '@ngx-translate/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class PrinterService {
  private headers: HttpHeaders;
  private endpointUrl: string;
  private translations: any;


  constructor(private http: HttpClient,
    private translateService: TranslateService
) {
  this.endpointUrl = AppConfig.endpoints.printers;
  this.headers = new HttpHeaders({'Content-Type': 'application/json'});
}

private handleError(error: any) {
  if (error instanceof Response) {
    return Observable.throw(error.json()['error'] || 'backend server error');
  }
  return Observable.throw(error || 'backend server error');
}


  public getAll(): Observable<Printer[]> {
    return this.http.get(this.endpointUrl).map(response => {
      return response;
    }).catch(error => this.handleError(error));
  }

  public getPrinterById(id: string): Observable<Printer[]> {
    return this.http.get(this.endpointUrl + '/' + id)
      .map(response => {
        return response;
      })
      .catch(error => this.handleError(error));
  }

}
