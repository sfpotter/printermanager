import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PrintersComponent} from './printers.component';
import { PrinterListComponent } from './printer-list/printer-list.component';
import { PrinterAddComponent } from './printer-add/printer-add.component';

const printersRoutes: Routes = [
  {
    path: '',
    component: PrintersComponent,
    children: [
      { path: '', component: PrinterListComponent},
      { path: 'add', component: PrinterAddComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(printersRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class PrintersRoutingModule {
}
