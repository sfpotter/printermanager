import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NavComponent} from './nav/nav.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [

  ],
  declarations: [

  ],
  providers: [
  ]
})

export class CoreModule {
  constructor() {
  }
}
