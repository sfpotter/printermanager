const restify = require('restify')



function respond (req, res, next) {

  const result = { sample: 'sample' }
      res.json(result)
      next()

}

 const server = restify.createServer()
 server.get('/add/:first/:second', respond)

server.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'X-Requested-With');
      return next();
 });


server.get('printers', (req, res, next) => {

  let printers = [
      { 
        name: 'Sample printer 1', 
        status: 1,
        ip: '192.168.1.0',
        description: 'Sample description', 
        netmask: '255.255.255.0',
        location: 'Sample location',
        connection: 'Socket'
      },


      { 
        name: 'Sample printer 2',
        description: 'Sample description',
        status: 2,
        ip: '192.168.1.1',
        netmask: '255.255.255.0',
        location: 'Sample location 2',
        connection: 'Socket'
      }
    ];

   res.json(printers);
 })





server.listen(8080, () => {
      console.log('%s listening at %s', server.name, server.url)
 });
